This is a sample project that can be used for learning the CICD implementation using
- AWS Codebuild
- AWS ECS fargate

Building this project

```gradle clean shadowJar```

Running this project

```java -jar /path/to/helloktor-1.0.jar```

Access http://localhost:8080 in the browser